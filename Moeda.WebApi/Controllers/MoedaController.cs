﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Moeda.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoedaController : Controller
    {
        /// <summary>
        /// METODO PARA CONVERTER MOEDA
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [Route("[action]")]
        [HttpGet("MoedaConverter")]
        public async Task<JsonResult> MoedaConverter(string from, string to, string amount)
        {
            string apiUrl = "https://rate-exchange-1.appspot.com/currency?from=" + from + "&to=" + to + "&q=" + amount;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);

                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();

                    return Json(data);
                }
            }

            return Json(new { });
        }

        /// <summary>
        /// METODO PARA PESQUISAR MOEDAS E SUAS COTAÇÕES 
        /// </summary>
        /// <param name="currencies"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        [Route("[action]")]
        [HttpGet("MoedaPesquisarCotacao")]
        public async Task<JsonResult> MoedaPesquisarCotacao(string currencies, string source)
        {
            string apiUrl = "http://apilayer.net/api/live?access_key=657c736c62cb02f3294f61189e971957&currencies="+ currencies + "&source="+ source + "&format=1";


            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(apiUrl);

                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return Json(data);
                }
            }

            return Json(new { });
        }
    }
}
