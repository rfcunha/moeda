﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Moeda.WebApp.Models
{
    public class MoedaViewModel
    {
        [Display(Name = "Moeda Corrente")]
        public string MoedaCorrente { get; set; }

        [Display(Name = "Moeda Cotar")]
        public string MoedaCotar { get; set; }

        [Required(ErrorMessage = "digite o valor")]
        [Display(Name = "Valor")]
        [MaxLength(10)]
        public string MoedaValor { get; set; }

        [Required(ErrorMessage = "selecione converter de")]
        [Display(Name = "Converter De")]
        public string MoedaFrom { get; set; }

        [Required(ErrorMessage = "selecione converter para")]
        [Display(Name = "Converter Para")]
        public string MoedaTo { get; set; }

    }
}
