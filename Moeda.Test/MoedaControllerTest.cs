﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Moeda.WebApi.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Moeda.Test
{
    public class MoedaControllerTest
    {
        private readonly MoedaController _moedaController;

        public MoedaControllerTest()
        {
            _moedaController = new MoedaController();
        }

        #region | MOEDA PESQUISAR |

        [Fact]
        public async Task Deve_Pesquisar_Cotacao_Todas()
        {
            JsonResult result = await _moedaController.MoedaPesquisarCotacao("","USD");

            Assert.NotNull(result);

            dynamic data = JObject.Parse(result.Value.ToString());

            var success = (string)data.SelectToken("success");

            Assert.Equal("True", success);
        }

        [Fact]
        public async Task Deve_Pesquisar_Cotacao_Expecifica()
        {
            JsonResult result = await _moedaController.MoedaPesquisarCotacao("BRL", "USD");

            Assert.NotNull(result);

            dynamic data = JObject.Parse(result.Value.ToString());

            var success = (string)data.SelectToken("success");

            Assert.Equal("True", success);
        }

        [Fact]
        public async Task Nao_Deve_Pesquisar_Cotacao()
        {
            JsonResult result = await _moedaController.MoedaPesquisarCotacao("", "BTL");

            Assert.NotNull(result);

            dynamic data = JObject.Parse(result.Value.ToString());

            var success = (string)data.SelectToken("success");

            Assert.Equal("False", success);
        }

        #endregion

        #region | MOEDA CONVERTER |

        [Fact]
        public async Task Deve_Converter_Moeda()
        {
            JsonResult result = await _moedaController.MoedaConverter("USD", "BRL", "1");

            Assert.NotNull(result);

            dynamic data = JObject.Parse(result.Value.ToString());

            var v = (string)data.SelectToken("v");

            Assert.True(!string.IsNullOrEmpty(v));
        }

        [Fact]
        public async Task Nao_Deve_Converter_Moeda()
        {
            JsonResult result = await _moedaController.MoedaConverter("BRL", "USDD", "1");

            Assert.NotNull(result);

            dynamic data = JObject.Parse(result.Value.ToString());

            var err = (string)data.SelectToken("err");

            Assert.True(!string.IsNullOrEmpty(err));
        }

        #endregion
    }
}
