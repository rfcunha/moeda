﻿$(document).ready(function () {

    MoedaPesquisarCotacao();
    MoedaConveter();

    $('#btnCotar').click(function () {
        MoedaPesquisarCotacao();
    });

    $('#btnConverter').click(function () {
        MoedaConveter();
    });
});

function MoedaConveter() {

    var url = "http://localhost/Moeda.WebApi/api/Moeda/MoedaConverter";

    $.get(
            url,
            {
                from: $('#MoedaFrom option:selected').val(),
                to: $('#MoedaTo option:selected').val(),
                amount: $('#MoedaValor').val()
            },
            function (data) {

                var resultado = JSON.parse(data);

                $('#lblResultado').html($('#MoedaFrom option:selected').val() + ": " + $('#MoedaValor').val() + " = <b>" + $('#MoedaTo option:selected').val()+"</b>: " + resultado.v);
        });

}

function MoedaPesquisarCotacao() {

    var currencies = $('#MoedaCotar option:selected').val();
    var source = $('#MoedaCorrente').val();

    var url = "http://localhost/Moeda.WebApi/api/Moeda/MoedaPesquisarCotacao";

    $.get(
            url,
        {
            currencies: currencies,
            source: source
        }
        ,
        function (data) {

            $('#tbodyTable').html('');

                $.each(JSON.parse(data).quotes, function (moeda, valor) {

                    $('#tbodyTable').append('<tr><td>' + moeda.substring(3, 6) + '</td><td>' + valor + '</td></tr>');
                });
        });

}